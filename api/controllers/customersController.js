'use strict';

var mongoose = require('mongoose'),
  customer = mongoose.model('Customer');


exports.list_all_customers = function(req, res) {
  customer.find({}, function(err, customer) {
    if (err)
      res.send(err);
    res.json(customer);
  });
};


exports.create_a_customer = function(req, res) {
  var new_customer = new customer(req.body);
  new_customer.save(function(err, customer) {
    if (err)
      res.send(err);
    res.json(customer);
  });
};

exports.read_a_customer = function(req, res) {
  // console.log("userid "+req.get('UserID'));
  customer.find({'userId': req.get('UserID')}, function(err, customer) {
    if (err)
      res.send(err);
    res.json(customer);
  });
};

exports.update_a_customer = function(req, res) {
  customer.findOneAndUpdate({userId: req.get('UserID')}, req.body, {new: true}, function(err, customer) {
    if (err)
      res.send(err);
    res.json(customer);
  });
};

exports.delete_a_customer = function(req, res) {
  customer.remove({
    userId: req.get('UserID')
  }, function(err, customer) {
    if (err)
      res.send(err);
    res.json({ message: 'customer successfully deleted' });
  });
};

exports.add_a_contract = function(req, res) {
  account.update({userId:req.get('UserID')}, {'$addToSet':{"contracts":req.body}}, function(err, account){
    if (err)
      res.send(err);
    res.json(account);
  });
};
