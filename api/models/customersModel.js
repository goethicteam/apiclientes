'use strict';


var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var CustomerSchema = new Schema({

  userId: {
    type: Number,
  },
  firstName: {
    type: String,
    Required: 'Client’s name'
  },
  surname: {
    type: String,
    Required: 'Client’s surname'
  },
  secondSurname: {
    type: String,
    Required: 'Client’s second surname'
  },
  sex: {
    type: String,
    Required: 'Client’s gender values are: MALE or FEMALE'
  },
  birthdate: {
    type: Date,
    Required: 'Client’s birth date'
  },
  identityDocument: {
    type: String,
    Required: 'Client’s identity'
  },
  address: {
    type: String,
    Required: 'Client’s address'
  },
  email: {
    type: String,
    Required: 'Client’s email'
  },
  avatar: {
    type: String
  },
  contracts:[
    {
      id: {
        type: String
      },
      type: {
        type: String,
        Required: 'Client’s name'
      },
      description: {
        type: String,
        Required: 'Client’s surname'
      },
      currency: {
        type: String,
        Required: 'Client’s second surname'
      }
    }
  ]
});

module.exports = mongoose.model('Customer', CustomerSchema);
