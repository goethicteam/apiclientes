'use strict';

module.exports = function(app) {
	var customers = require('../controllers/customersController');

	// todoList Routes
	app.route('/customers')
		.get(customers.list_all_customers)
		.post(customers.create_a_customer);

	app.route('/customers/:userId')
		.get(customers.read_a_customer)
		.put(customers.update_a_customer)
		.delete(customers.delete_a_customer)
		.patch(customers.add_a_contract);
		

};
